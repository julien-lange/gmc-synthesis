module SimplePath where

import Data.Set as S
import Data.List as L
import Data.Map.Lazy as M
import Data.Tree as T
import Data.Foldable as F
import Data.Either
import Data.Maybe

import System.IO.Unsafe
import Debug.Trace


--
-- This modules contains functions to generate simple paths in a graph
-- /!\ Some of these functions need to made (much) more efficient,
-- some generate the same paths several times.
--


--
-- Find all the simple paths that starts from 'src'
--
treeSimplePaths :: (Ord b, Ord c, Show b, Show c) =>
                   ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b))
                   -> (Set b, b, Set c, Set (b,c,b))
                   -> b 
                   -> Forest c
treeSimplePaths f ts src = dfs src []
  where
   filterEdge visited current (label,node) = not $ L.elem (current,label,node) visited
   --
   dfs current visited = let newpairs = L.nub $ L.map (\(x,y) -> (current,x,y)) $
                                        L.filter (\edge -> filterEdge visited current edge) (S.toList $ f ts current)
                             newforest = L.map (\e@(n,l,n') -> Node l (dfs n' (e:visited))) newpairs
                         in newforest
                            



--
-- Find all the simple paths that starts from 'src' and terminate at 'target'
-- TO BE USED ONLY TO CHECK EXISTENCE OF COMMON PATH BETWEEN MERGING NODES!
-- i.e., if "target" is *not* reachable from "src", it will raise an error.
--
treeSimplePathsBetween ::  (Ord b, Ord c, Show b, Show c) =>
                   ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b))
                   -> (Set b, b, Set c, Set (b,c,b))
                   -> b -> b
                   -> Forest c
treeSimplePathsBetween f ts src target =
  let
    res = (dfs src [])
  in case res of 
    Just forest -> forest
    Nothing -> error $ "Something went terribly wrong in finding a path between "++(show src)++" and "++(show target)
  where
   filterEdge visited current (label,node) = not $ L.elem (current,label,node) visited
   --
   dfs current visited 
     | current == target = Just []
     | otherwise = 
       let newpairs = L.nub $ L.map (\(x,y) -> (current,x,y)) $
                      L.filter (\edge -> filterEdge visited current edge) (S.toList $ f ts current)
           newforest = L.map (\(l,(Just f)) -> Node l f) $
                       L.filter (isJust . snd) $
                       L.map (\e@(n,l,n') -> (l,(dfs n' (e:visited)))) newpairs
       in 
        if L.null newforest
          then Nothing 
          else Just newforest
   
listSimplePathsBetween :: (Ord b, Ord c, Show b, Show c) =>
                         ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b))
                         -> (Set b, b, Set c, Set (b,c,b))
                         -> b -> b
                         -> [[c]]
listSimplePathsBetween f ts src target =
  let
    res = (traverse src [] [] M.empty)
  in case res of 
    Just forest -> forest
    Nothing -> error $ "Something went terribly wrong in finding a path between "++(show src)++" and "++(show target)
  where
   filterEdge visited current (label,node) = not $ L.elem (current,label,node) visited
   --
   traverse current acc visited currentmap
     | current == target = Just [(reverse acc)]
     | otherwise = 
       let newpairs = L.nub $ L.map (\(x,y) -> (current,x,y)) $
                      L.filter (\edge -> filterEdge visited current edge) (S.toList $ f ts current)
           newforest = L.foldr (++) [] $
                       L.map (\(Just f) -> f) $
                       L.filter (isJust) $
                       L.map (\e@(n,l,n') -> ((traverse n' (l:acc) (e:visited) currentmap))) newpairs
           newmap = M.insert (current, acc) newforest
       in 
        if L.null newforest
          then Nothing 
          else Just newforest
     


--
-- Find all the simple paths that starts from 'src'
--
listSimplePaths :: (Ord b, Ord c, Show b, Show c) =>
                   ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b))
                   -> (Set b, b, Set c, Set (b,c,b))
                   -> b 
                   -> [[(b,c,b)]]
listSimplePaths f ts src = dfs src []
  where
   filterEdge visited current (label,node) = not $ L.elem (current,label,node) visited
   --
   addHead h = L.map (\x -> h:x)
   --
   dfs current visited = let newpairs = L.nub $ L.map (\(x,y) -> (current,x,y)) $
                                          L.filter (\edge -> filterEdge visited current edge) (S.toList $ f ts current)
                             newforest = 
                                 L.foldr (++) [] $
                                 L.map (\e@(n,l,n') -> addHead e (dfs n' (e:visited)) ) newpairs
                           in if L.null newforest
                              then [[]]
                              else newforest
                                   

   
   
listSimplePathsEvt :: (Ord b, Ord c, Show b, Show c) =>
                      ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b))
                      -> (Set b, b, Set c, Set (b,c,b))
                      -> b 
                      -> [[c]]   
listSimplePathsEvt f ts src = L.map (\x -> L.map (\(n,e,n') -> e) x) $ listSimplePaths  f ts src
                   
-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
optSimplePathsBetween :: (Ord b, Ord c, Show b, Show c) =>
                         ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b))
                         -> (Set b, b, Set c, Set (b,c,b))
                         -> b -> b
                         -> [[c]]
optSimplePathsBetween f ts src target =
  let
    res =  fst $ traverse src [] M.empty
  in case res of 
    Just paths -> L.map (L.map (\(x,y,z) -> y)) paths
    Nothing -> error $ "Something went terribly wrong in finding a path between "++(show src)++" and "++(show target)
  where
    filterPath es = let goodpath p = F.and $ L.map (\e -> not $ e `L.elem` p) es
                    in L.filter goodpath
    --
    makeMaybe xs = if L.null xs then Nothing else Just xs
    --
    consPaths l Nothing = Nothing
    consPaths l (Just ps) = Just $ L.map (\x -> x++[l]) ps
    --
    traverse current acc currentmap
     | current == target = (Just [acc], currentmap)
     | otherwise =
       case M.lookup current currentmap of
         Just maybePaths -> case maybePaths of
           Nothing -> (Nothing, currentmap)
           Just paths -> (makeMaybe $ filterPath acc paths, currentmap)
         Nothing -> let newpairs = L.map (\(x,y) -> (current,x,y)) (S.toList $ f ts current)
                        newmap = doNodes newpairs acc currentmap
                        fromCurrent = makeMaybe $
                                      L.foldr (++) [] $
                                      L.map (\(Just x) -> x) $
                                      L.filter isJust $
                                      L.map (\e@(x,y,z) -> consPaths e (newmap!z)) newpairs
                    in (fromCurrent, M.insert current fromCurrent currentmap)
    --
    doNodes (e@(n,l,n'):xs) acc currentmap = 
      let (paths, newmap) = traverse n' (e:acc) currentmap
      in doNodes xs acc newmap
    doNodes [] acc currentmap = currentmap




--
-- Check whether there is a comon path in two forests
--
commonPath :: (Eq a) => Forest a -> Forest a -> Bool
commonPath f1 f2 = 
  (f1 == f2)
  ||
  (F.or $ L.map (\(x,y) -> commonPathInTree x y) (cart f1 f2))
  where 
    commonPathInTree (Node e1 t1) (Node e2 t2) = (e1 == e2) && (commonPath t1 t2)
    cart xs ys = [(x,y) | x <- xs, y <- ys]

   
        
        
        


-- simplePathsState :: (Ord b, Ord c, Show b, Show c) => 
--                     ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b)) -> (Set b, b, Set c, Set (b,c,b)) -> b -> b -> [[b]]
-- simplePathsState f sys s1 s2 =  L.map (L.map snd) (simplePathsPairs f sys s1 s2)

-- simplePathsLabel :: (Ord b, Ord c, Show b, Show c) => 
--                     ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b)) -> (Set b, b, Set c, Set (b,c,b)) -> b -> b -> [[c]] 
-- simplePathsLabel f sys s1 s2 = L.map (L.map fst) (simplePathsPairs f sys s1 s2)


-- simplePathsPairs :: (Ord b, Ord c, Show b, Show c) =>
--                     ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b)) ->
--                     (Set b, b, Set c, Set (b,c,b)) -> b -> b -> [[(c, b)]]
-- simplePathsPairs = simplePathBetweenList    



-- --
-- -- Note that his function should only be called if src/=target
-- -- which is always the case since it is called on a "not involved participant"
-- -- there the choice is not a self-loop
-- --
-- simplePathBetweenList :: (Ord b, Ord c, Show b, Show c) =>
--                      ((Set b, b, Set c, Set (b,c,b))-> b -> Set (c,b))
--                      -> (Set b, b, Set c, Set (b,c,b))
--                      -> b -> b
--                      -> [[(c, b)]]
-- simplePathBetweenList f ts@(nodes,init,labs,trans) src target =  
--   fst (dfs src S.empty M.empty)
--   where dfs current visited map 
--           | current == target = let path = ([[]]) in (path, M.insert current path map)
--           | S.member current visited = let path = [] in (path, M.insert current path map)
--           | otherwise = case M.lookup current map of
--             Just paths -> (paths, map)
--             Nothing -> let newpairs = f ts current
--                            newstates = S.map snd newpairs
--                            newmap = bfs (S.toList newstates) (S.insert current visited) map
--                            fromCurrent = L.foldr (++) [] $ L.map (\(x,y) -> addHead (x,y) (newmap!y)) (S.toList newpairs)
--                        in (fromCurrent, M.insert current fromCurrent newmap)
--         --
--         addHead h = L.map (\x -> h:x)
--         --
--         bfs (n:ns) seen map =
--           let
--             (paths , newmap) = dfs n seen map
--           in 
--            bfs ns seen newmap
--         bfs [] _ map = map
                   
    
    
     


-- allSimplePathsLabel :: (Ord b, Ord c, Show b, Show c) => 
--                     ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b)) -> (Set b, b, Set c, Set (b,c,b)) -> b -> [[c]]
-- allSimplePathsLabel f sys s1 = L.map (L.map fst) (allSimplePaths f sys s1)


-- allSimplePaths :: (Ord b, Ord c, Show b, Show c) =>
--                     ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b)) -> (Set b, b, Set c, Set (b,c,b)) -> b  -> [[(c, b)]]
-- allSimplePaths f ts@(nodes,init,labs,trans) src =
--   let res = simplePathBetweenList f ts src src
--   in trace ("from:"++(show src)++"\n\n"++(show res)) res



-- allPaths :: (Ord b, Ord c, Show b, Show c) =>
--                     ((Set b, b, Set c, Set (b,c,b)) -> b -> Set (c,b)) 
--                     -> (Set b, b, Set c, Set (b,c,b))
--                     -> b  
--                     -> [[(b,c, b)]]
-- allPaths f machine@(nodes,init,labs,trans) q0 = 
--   let sf = (fst (helper q0 S.empty M.empty))
--   in
--    -- trace 
--    -- ("Simple paths from "++(show q0)++"\n"++(show sf))
--    sf
--   where helper q seen map 
--           | (S.size (f machine q) == 0) = ([[]] , map)
--           | S.member q seen = ([[]], map)
--           | otherwise = case M.lookup q map of
--                    Just paths -> (paths, map)
--                    Nothing -> let pairs = (f machine q)
--                                   nextstates = S.map snd pairs
--                                   newmap = doStates (S.toList nextstates) (S.insert q seen) map
--                                   fromq = L.foldr (++) [] $ L.map (\(x,y) -> addHead (q,x,y) (newmap!y)) (S.toList pairs)
--                               in (fromq, newmap) 
--         --
--         addHead h = L.map (\x -> h:x)  
--         --
--         doStates (q:qs) done map =
--           let
--             (ps , mps) = helper q done map
--           in doStates qs (S.union done (M.keysSet mps)) (M.insert q ps mps)
--         doStates [] _ map = map