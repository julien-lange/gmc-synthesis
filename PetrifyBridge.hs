module PetrifyBridge where

import Data.Set as S
import CFSM
import TS
import Misc

--
-- This modules contains function to output files compatible with Petrify
--


state2petrify :: State -> String
state2petrify s = rmveQuotes $ (show s)

event2petrify (q1,q2,m1,m2,msg) =
  let st_arrow = "AAA"
      st_coma = "CCC"
      st_colon = "COCO"
  in rmveQuotes(
    (state2petrify q1)++st_coma++
    (state2petrify q2)++st_coma++
    (show m1)++st_arrow++
    (show m2)++st_colon++
    (show msg)
    )

ts2petrify :: TS -> String
ts2petrify (nodes,initnode,events,trans) =
  let st_nodes = ".outputs "++(S.fold (++) "" (S.map (\x -> (event2petrify x)++" ") events))++"\n"
      st_init = ".marking {"++(printNodeId initnode)++"}\n"
      st_trans = S.fold (++) "" (
        S.map (
           \(x,y,z) -> (printNodeId x)
                       ++" "++
                       (event2petrify y)
                       ++" "++
                       (printNodeId z)
                       ++"\n"
              )
        trans
        )
  in st_nodes++".state graph \n"++st_trans++st_init++".end\n"
   