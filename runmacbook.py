#!/usr/bin/python

import sys
import subprocess
import os
import os.path
import string


# HASKELL TOOLS
GMC = "./GMC"
BG = "./BuildGlobal"

# EXTERNAL TOOLS
HKC = "./hknt-1.0/hkc"
PETRY = "/Users/jlange/Research/petrify-4.2-mac/petrify-intel"
DRAW = "/Users/jlange/Research/petrify-4.2-mac/petrify-intel"

# WORKAROUND LACK OF escape character in PETRIFY
st_arrow = "AAA"
st_coma = "CCC"
st_colon = "COCO"


#print "[PY] Executing ",GMC," ",sys.argv[1],"..."
subprocess.check_call([GMC,sys.argv[1]]) #  ,"+RTS","-p"])

mybasename = "outputs/"+os.path.basename(sys.argv[1])

with open('.machinenumber') as f:
    machine_number = int(f.readline())

prebool = True
for i in range(machine_number):
    #print "Testing machine "+str(i)+" for language equivalence"
    cmd = subprocess.Popen(
        [HKC,"-equiv",mybasename+"_machine_"+str(i),mybasename+"_projection_"+str(i)], stdout=subprocess.PIPE)
    for line in cmd.stdout:
        spa = line.split('<<<')
        if len(spa) > 1:
            spb = spa[1].split('>>>')
            prebool = prebool and (spb[0].strip() == "true")
            #print "Is machine "+str(i)+" representable? "+spb[0].strip()

print "Is the system Language-equivalence Representable (part (i))? ***"+str(prebool)+"***"
print ""

#print "[PY] Generating Diagrams..."
subprocess.call(["dot","-Tpng",mybasename+"_machines.dot","-o",mybasename+"_a_machines.png"])
subprocess.call(["dot","-Tpng",mybasename+"_ts.dot","-o",mybasename+"_b_ts.png"])

# TS TO PETRIFY
#print "Calling petrify..."
subprocess.check_call([PETRY,"-dead","-ip","-efc",mybasename+"_toPetrify","-o","tempefc"])
#print "Drawing EFC PN..."
#subprocess.call([DRAW,"-noinfo","tempefc","-ip","-bw","-Tdot","-o",mybasename+"_efcp.dot"])

replacements = [(st_coma,','), (st_arrow,'->'),(st_colon,':')]

# TO GENERATE PN VIA PETRIFY
#with open(mybasename+"_efc.dot", "wt") as fout:
#    with open(mybasename+"_efcp.dot", "rt") as fin:
#        for line in fin:
#            for (src, target) in replacements:
#                line = line.replace(src,target)
#            fout.write(line)
#fout.close()
#fin.close()
#subprocess.call(["dot","-Tpng",mybasename+"_efc.dot","-o",mybasename+"_pn.png"])

# PETRIFY BACK TO HASKELL
with open(mybasename+"_petrinet", "wt") as fout:
    with open("tempefc", "rt") as fin:
        for line in fin:
            for (src, target) in replacements:
                line = line.replace(src,target)
            fout.write(line)
fout.close()
fin.close()


subprocess.check_call([BG,mybasename+"_petrinet"])
#subprocess.call(["dot","-Tpng",mybasename+"_petrinet_finalpn.dot","-o",mybasename+"_finalpn.png"])
#subprocess.Popen(["dot","-Tpng",mybasename+"_petrinet_preglobal.dot","-o",mybasename+"_preglobal.png"], stderr=subprocess.PIPE)
subprocess.Popen(["dot","-Tpng",mybasename+"_petrinet_global.dot","-o",mybasename+"_c_global.png"], stderr=subprocess.PIPE)
