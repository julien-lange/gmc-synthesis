module MultiplyCS where

import Prelude hiding (succ)
import Data.Set as S
import Data.List as L
import Misc as M
import CFSM


--
-- This module is useful to generate bigger systems
--

multiply ::  Int -> System -> System
multiply i sys
  | i == 0 = sys
  | otherwise = (multiply (i-1) sys)++(indexCS sys (i*(length sys)) )


indexCS :: System -> Int -> System
indexCS cs i = L.map (\x -> indexCFSM x i) cs

indexCFSM :: CFSM -> Int -> CFSM
indexCFSM (states, init, acts, trans) i = (states, init, newacts , newtrans)
  where newacts = S.map updateaction acts
        newtrans = S.map (\(x,y,z) -> (x, updateaction y, z)) trans
        updateaction (dir,chan,msg) = (dir,indexChan chan i, msg)
        
indexChan :: Channel -> Int -> Channel
indexChan (m1, m2) i = (m1+i,m2+i)
  -- where concatenate x y pow 
  --         | y >= pow =  concatenate x y (pow*10)
  --         | otherwise = x * pow + y