module Misc where
import Data.List as L
import Data.Set as S

rmveQuotes :: String -> String            
rmveQuotes (x:xs) =
    if x == '\"' then rmveQuotes xs
    else x : (rmveQuotes xs)
rmveQuotes [] = ""  
         
transitiveClosureParam :: (Eq a) => (a -> a -> Bool) -> [(a, a)] -> [(a, a)]
transitiveClosureParam f closure 
  | closure == closureUntilNow = closure
  | otherwise                  = transitiveClosureParam f closureUntilNow
  where closureUntilNow = 
          L.nub $ closure ++ [(a, c) | (a, b) <- closure, (b', c) <- closure, ((f b b') || (f b' b))]
          


-- Assuming that (x,x) in ys for all x
equivalenceRelation :: (Eq a) => [(a,a)] -> [(a,a)]
equivalenceRelation ys = transitiveClosure (L.nub $ addPairs ys)
  where addPairs ((x,y):xs) =  if x/=y
                               then (x,y):((y,x):(addPairs xs))
                               else (y,x):(addPairs xs)
        addPairs [] = []
        

equivalenceClass :: (Eq a, Ord a) => Set (a,a) ->  a -> Set a
equivalenceClass rel z = S.fold (S.union) S.empty $ 
                         S.map (\(x,y) -> S.insert x (S.singleton y)) (S.filter (\(x,y) -> x == z || y ==z) rel)
          
-- http://stackoverflow.com/questions/19212558/transitive-closure-from-a-list-using-haskell
transitiveClosure :: (Eq a) =>  [(a, a)] -> [(a, a)]
transitiveClosure closure 
  | closure == closureUntilNow = closure
  | otherwise                  = transitiveClosure closureUntilNow
  where closureUntilNow = 
          L.nub $ closure ++ [(a, c) | (a, b) <- closure, (b', c) <- closure, b == b']

writeToFile :: FilePath -> String -> IO()
writeToFile file content = writeFile file content


justFilter :: (Ord a) => Set (Maybe a) -> Set a
justFilter set = helper (S.toList set)
  where helper (x:xs) = case x of 
          Just y -> S.insert y (helper xs)
          Nothing -> helper xs
        helper [] = S.empty
        
        
cartProd :: (Ord a, Ord b) => Set a -> Set b -> Set (a,b)
cartProd sa sb = S.fromList $ helper (S.toList sa) (S.toList sb)
  where helper xs ys = [(x,y) | x <- xs, y <- ys]


